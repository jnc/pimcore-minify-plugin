Minify Plugin
=============================

JNC Tech has forked this repo from Basilicom.  The original repo can be found at [on their github](https://github.com/basilicom/pimcore-plugin-minify). Thank you for your help! The main change made
is now the files are named according to the hash of their contents, rather than their filenames.

This means that if the file contents change, a new hash is generated forcing the browser to load a new file
instead of pulling the old one from cache. We've also eliminated the cache timestamp as it makes debugging hard.

I've kept much of their documentation as supplied below, but updated where it makes sense.

## Synopsis

This plugin adds two new classes to help with with CSS and Javascript
combination / minification in layouts / templates.

## Installation

To install the plugin, first add the following repository to your `composer.json` file:

```javascript
"repositories": [
    {
      "type": "vcs", 
      "url": "https://bitbucket.org/jnc/pimcore-minify-plugin.git"
    }
  ]

```

Add the "jnctech/pimcore-plugin-minify" requirement to the composer.json 
in the toplevel directory of your pimcore installation:

Example:

    {
        "require": {
            "jnctech/pimcore-plugin-minify": ">=1.0.0"
        }
    }


Or run:

    composer require jnctech/pimcore-plugin-minify

Then enable and install the Minify plugin in Pimcore Extension Manager (Extras > Extensions).

## Integration

Add to your templates:

        <?php
            $js = new \JNCTech\PimcoreMinify\Js();
            echo $js->add("/website/static/js/jquery.js")
                ->add("/website/static/js/site.js")
                ->get();
        ?>
        
Or for CSS:

        <?php
            $js = new \JNCTech\PimcoreMinify\Css();
            echo $js->add("/website/static/css/bootstrap.css")
                ->add("/website/static/icons/icons.css")
                ->add("/website/static/css/site.css")
                ->get();
        ?>

## Mode of operation

If Pimcore operates in the DEBUG mode, JS/CSS files are not minified, but served
directly from the filesystem.

In production mode, CSS and JS files are combined, minified, and written
to the ``/website/var/tmp/`` directory as ``pimcore_minify_plugin_*`` files. Each set
of files gets their own cache file based on the md5 sum of the combined
filenames. For CSS files, relative urls/paths are automatically rewritten.

You can force regeneration of the minified files by adding a ``MINIFY_REFRESH``
request parameter. Example: ``http://localhost/test/?MINIFY_REFRESH``

## License

GNU General Public License version 3 (GPLv3)

## Release Notes

### v1.1.0
* This is the first JNC revision of the plugin. Files are minified and a unique filename cache is generated based on the contents. This prevents the browser caching old minified files.
* The timestamps are no longer included files when in debug mode, as that was causing issues keeping breakpoints when debugging on the client side.