<?php

namespace JNCTech\PimcoreMinify;

abstract class Core 
{
    const FORCE_REFRESH = 'MINIFY_REFRESH'; 
    const MINIFIED_MAP = PIMCORE_PLUGINS_PATH . '/PimcorePluginMinify/lib/PimcoreMinify/minified-map.json';

    protected $filenames = [];
    protected $errors = [];
    
    private $suffix = null;

    public function add($filename){
        if ($this->suffix === null) {
            $this->suffix = pathinfo($filename, PATHINFO_EXTENSION);
        }
        
        if (file_exists(PIMCORE_DOCUMENT_ROOT . $filename)) {
            $this->filenames[] = $filename;
        } else {
            $this->errors[$filename] = "File does not exist.";
        }
        return $this;
    }

    public function get(){
        if (\Pimcore::inDebugMode()) {
            return $this->getDebug();
        }
        return $this->getMinified();
    }

    abstract protected function getMinified();
    abstract protected function getDebug();
    abstract protected function getExtension();

    /**
     * determine the cache key based on the filenames
     */
    protected function getCacheKey(){
        return md5(implode($this->filenames));
    }

    protected function getMinifiedUrl(){
        if(!file_exists(Core::MINIFIED_MAP)){
            throw new \Exception("minified-map.json does not exist!");
        }

        $minifiedMap = json_decode(file_get_contents(Core::MINIFIED_MAP));

        $fileKey = $this->getFileKey();

        $file = $minifiedMap->$fileKey;

        if(!$file){
            throw new \Exception("minified-map.json does not contain key!");
        }

        return $file;
    }

    protected function existsMinifiedFile(){
        // regenerate always, if we get the force parameter ..
        if (array_key_exists(Core::FORCE_REFRESH, $_REQUEST)) {
            return false;
        }

        try{
            return (file_exists(PIMCORE_TEMPORARY_DIRECTORY . '/' . $this->getMinifiedUrl()));
        }
        catch(\Exception $e){
            return false;
        }
    }

    /**
     * Take the minified contents of the file group and write it to a file in the PIMCORE_TEMPORARY_DIRECTORY
     * where the filename is the md5 of the file contents. This filename will be mapped in the minified-map.json
     * file where the key will be the md5 of all file names in the group. 
     *
     * @param string $fileContent
     * @return void
     */
    protected function writeMinifiedFile($fileContent){
        $minifiedMap = new \stdClass();
        
        if(file_exists(Core::MINIFIED_MAP)){
            $minifiedMap = json_decode(file_get_contents(Core::MINIFIED_MAP));
        }

        //create dictionary key from file names
        $fileKey = $this->getFileKey();
        $fileName = 'pimcore_minify_plugin_' . md5($fileContent) . $this->getExtension();
    
        //write the minified file
        file_put_contents(PIMCORE_TEMPORARY_DIRECTORY . '/' . $fileName, $fileContent);

        //update the minified map
        $minifiedMap->$fileKey = $fileName;
        file_put_contents(Core::MINIFIED_MAP, json_encode($minifiedMap));

    }

    /**
     * Returns a hash of all filenames to serve as the key for dictionary lookup for file.
     *
     * @return void
     */
    protected function getFileKey(){
        return md5(implode($this->filenames));
    }
}