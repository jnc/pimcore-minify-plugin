<?php

namespace JNCTech\PimcoreMinify;

use Pimcore\API\Plugin as PluginLib;

/**
 * The plugin operates by allowing groups of files to be minified together,
 * either groups of JS files or groups of CSS files (both cannot be grouped together).
 * 
 * For each group, the files are individually read, wrote into a single file, and then the file is named
 * as the md5 of the filecontents.  
 * 
 * Groups are identified by taking all filenames in the group, concatenating them together, and then generating
 * the md5 of the filenames. Any time a group is requested, this md5 is computed and looked for in 
 * the minified-map.json file which is generated in the plugin.
 * 
 * This map is maintained when files are minified for the first time, or if explicitly requested.
 * The file is simply a dictionary mapping md5(filenames) -> md5(minifiedfile). The key is used to look up the minified
 * file that has been generated for the group, and then the minified file is included in the HTML document.
 */
class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    public function init()
    {
        parent::init();

         // using methods
         \Pimcore::getEventManager()->attach("system.startup", [$this, "cleanupOnRefresh"]);
    }

    public function cleanupOnRefresh(){
         // regenerate always, if we get the force parameter ..
         if (array_key_exists(Core::FORCE_REFRESH, $_REQUEST)) {
            //delete all temp files
            $generatedFiles = glob(PIMCORE_TEMPORARY_DIRECTORY . "/" . "pimcore_minify_plugin_*");

            foreach($generatedFiles as $tempFile){
                unlink($tempFile);
            }

            if(file_exists(Core::MINIFIED_MAP)){
                unlink(Core::MINIFIED_MAP);
            }
        }
    }

    public static function install()
    {
        return true;
    }

    public static function uninstall()
    {
        return true;
    }

    public static function isInstalled()
    {
        return true;
    }
}
